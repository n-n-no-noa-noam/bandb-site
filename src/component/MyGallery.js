import PhotoAlbum from "react-photo-album";


const photos = [
    { src: "./images/1.jpg", width: 800, height: 600 },
    { src: "./images/2.jpg", width: 1600, height: 900 },
    { src: "./images/3.jpg", width: 800, height: 600 },
    { src: "./images/4.jpg", width: 1600, height: 900 },
    { src: "./images/5.jpg", width: 800, height: 600 },
    { src: "./images/6.jpg", width: 1600, height: 900 },
    { src: "./images/7.jpg", width: 800, height: 600 },
    { src: "./images/8.jpg", width: 1600, height: 900 },
    { src: "./images/9.jpg", width: 800, height: 600 },
    { src: "./images/10.jpg", width: 1600, height: 900 },
    { src: "./images/11.jpg", width: 800, height: 600 },
    { src: "./images/12.jpg", width: 1600, height: 900 },
    { src: "./images/13.jpg", width: 800, height: 600 },
    { src: "./images/14.jpg", width: 1600, height: 900 },
    { src: "./images/15.jpg", width: 800, height: 600 },
    { src: "./images/16.jpg", width: 1600, height: 900 },
];

export default function MyGallery() {
    return (
            <section id ="galry">
                <PhotoAlbum  layout="rows" photos={photos} />;
            </section>
        )
}