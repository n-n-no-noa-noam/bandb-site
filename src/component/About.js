



export default function About() {
    return (
        <section id="about">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2 id="about-titel">קצת על הצימר</h2>
                        <p class="lead">ברוכים הבאים לצימר "בקתה מול הנוף", מתחם האירוח עוצר הנשימה, בו יש נוף פנורמי מדהים מהכנרת והגולן עד החרמון</p>
                        <i>
                                במיקום נוח במרחק של פחות מחצי שעה נסיעה מרמת הגולן חצור וטבריה 
                                הצימר שלנו מציע מפלט שליו הרחק מחיי העיר הגועשים, זה הזמן לקחת הפסקה ולבוא להינות משלוות הטבע ממרגוע לנפש, משקט נעים ציוץ ציפורים ומזריחה שצובעת את השמים בגוונים מרהיבים
                                ואם זה לא מספיק ממוקם במרחק של דקות ספורות מנחלים  קסומים ושמורות טבע
                                <br></br>
                                <br></br>
                                בצימר תוכלו ליהנות ממתחם אישי ומטופח
                                <br></br>
                                <br></br>
                                בין אם אתם מחפשים חופשה רומנטית, חופשה משפחתית או בריחה בודדת, הצימר שלנו מספק את המקלט המושלם עבורכם
                                <br></br>
                                בצימר שלנו, אנו מתגאים במתן אירוח ללא דופי ושירות אישי. הצוות המסור שלנו מחויב להבטיח שהשהות שלכם תהיה לא פחות מיוצאת דופן, לתת מענה לכל צורך שלכם ולעלות על הציפיות שלכם
                                <br></br>
                                חוו את ההרמוניה של הטבע, את הפיתוי של נופים עוצרי נשימה, ואת החום של אירוח אמיתי בצימר שלנו. הזמינו את שהותכם אצלנו עוד היום וצרו זיכרונות שיימשכו לכל החיים
                                <br></br>
                                ואיפה כל הטוב הזה נמצא
                                נוף כנרת רחוב נוף הגולן 22/1 בין ראש פינה לצפת מול מלון מצפה הימים, בסמוך לגן שעשועים ובקרבת בית כנסת
                                <br></br>
                                התפנקו מהנוחות והנוחיות של הצימר המעודן שלנו. במשכן המזמין שלנו, תמצאו מגוון של מאפיינים יוקרתיים כדי לשפר את שהותכם. תיהנו  ממים חמים על גז, ג'קוזי מרגיע, או טבילה מרעננת בבריכה. כמו כן שולחן הפינג פונג לתחרות ידידותית
                                <br></br>
                                הכינו ארוחות בקלות באמצעות המטבח המאובזר שלנו, הכולל מיקרוגל, מקרר, פלטת חימום וכיריים לצרכי הבישול שלכם
                                קפה תה וסוכר ומכונת קפה.   במתחם ישנה פינת ברביקיו לחווית חופשה מושלמת. שטחי הדשא המרווחים מספקים מקום בשפע להירגע וליהנות מהסביבה וממיטות השיזוף
                                <br></br>
                                גלו את הפינוקים הללו ועוד בצימר יוצא הדופן שלנו. חוו שילוב הרמוני של נוחות ורוגע במהלך שהותכם בצימר "בקתה מול הנוף"
                        </i>
                    </div>
                </div>
            </div>
        </section>
    );
}
  






