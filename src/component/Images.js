import Carousel from 'react-bootstrap/Carousel';

export default function Images(){

    return(

<Carousel slide={false}>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/10.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>


      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/19.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>


      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/2.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>



      <Carousel.Item>
        <img
          className="d-block w-100"
          src="images/9.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>


    </Carousel>
      
    )
} 