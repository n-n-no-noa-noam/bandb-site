


export default function Nav() {
    return (
        <nav  className="navbar navbar-expand-lg  fixed-top" id="mainNav">
            <div className="container px-4">

                <a className="navbar-brand" href="#page-top">ביקתה בנוף</a>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
                
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ms-auto">
                        <li className="topright nav-item"><a className="nav-link" href="#about">הסבר קצר</a></li>
                        <li className="topright nav-item"><a className="nav-link" href="#services">יצירת קשר</a></li>
                        <li className="topright nav-item"><a className="nav-link" href="#ToArrive">דרכי הגעה</a></li>
                        <li className="topright nav-item"><a className="nav-link" href="#galry">גלריה</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}
  



