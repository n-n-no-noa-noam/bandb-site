




export default function ToArrive() { 
    return (
        <div id="ToArrive" className="ToArrive">
            <h1 className="title">דרכי הגעה</h1>
            <iframe title = "map" className="maps" src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3348.020222907015!2d35.529654870447!3d32.95047548241957!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMzLCsDU3JzAwLjYiTiAzNcKwMzEnNDcuOCJF!5e0!3m2!1siw!2sil!4v1687350640468!5m2!1siw!2sil" loading="lazy" allowfullscreen="" referrerpolicy="no-referrer-when-downgrade"></iframe>
            
            <div className="toArrive-link">
                <p className="maps-p">
                    נןף הגןלן 22\1 צפת
                    <img className="img" src="./images/maps.png"/>
                </p>
                <p className="waze-p">
                    <a className="link" href="https://www.waze.com/en/live-map/directions/%D7%A6%D7%A4%D7%AA?latlng=32.950483629917926%2C35.529540538918816">קישור לוויז</a>
                    <a  href="https://www.waze.com/en/live-map/directions/%D7%A6%D7%A4%D7%AA?latlng=32.950483629917926%2C35.529540538918816"><img className="img" src="./images/waze-icon.png"/></a>
                </p>
                
            </div>

        </div>
    );
}
  






