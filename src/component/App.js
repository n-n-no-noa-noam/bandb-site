
import Nav from "./Nav"
import Images from "./Images";
import About from "./About";
import "./App.css"
import Contact from "./Contact";
import MyGallery from "./MyGallery";
import ToArrive from "./ToArrive";


export default function App() {
  return (
    <div className="App">
      <Nav/>
      <Images/>
      <About/>
      <Contact/>
      <ToArrive/>
      <MyGallery/>
    </div>
  );
}
